#!/usr/bin/python3
import tkinter as tk
import tkinter.ttk as ttk


class GuiApp:
    def __init__(self, master=None):
        # build ui
        Toplevel_1 = tk.Tk() if master is None else tk.Toplevel(master)
        Toplevel_1.configure(height=200, width=200)
        Toplevel_1.title("Grid Calculator")
        Labelframe_1 = ttk.Labelframe(Toplevel_1)
        Labelframe_1.configure(height=200, text='Grid Parameters', width=200)
        Label_1 = ttk.Label(Labelframe_1)
        Label_1.configure(state="normal", text='Focal Dist. (cm):')
        Label_1.grid(column=0, row=0, sticky="e")
        Label_2 = ttk.Label(Labelframe_1)
        Label_2.configure(text='SID (cm):')
        Label_2.grid(column=0, row=1, sticky="e")
        Label_3 = ttk.Label(Labelframe_1)
        Label_3.configure(text='Grid Ratio:')
        Label_3.grid(column=0, row=2, sticky="e")
        Label_4 = ttk.Label(Labelframe_1)
        Label_4.configure(text='Center-Edge Dist. (cm):')
        Label_4.grid(column=0, row=3, sticky="e")
        Label_5 = ttk.Label(Labelframe_1)
        Label_5.configure(text='Grid-FPD Misallign. (cm):')
        Label_5.grid(column=0, row=4, sticky="e")
        self.Entry_f0 = ttk.Entry(Labelframe_1)
        self.focal_dist = tk.DoubleVar()
        self.Entry_f0.configure(
            justify="center",
            textvariable=self.focal_dist,
            width=10)
        self.Entry_f0.grid(column=1, row=0)
        self.Entry_sid = ttk.Entry(Labelframe_1)
        self.sid = tk.DoubleVar()
        self.Entry_sid.configure(
            justify="center",
            textvariable=self.sid,
            width=10)
        self.Entry_sid.grid(column=1, row=1)
        self.Entry_r = ttk.Entry(Labelframe_1)
        self.ratio = tk.DoubleVar()
        self.Entry_r.configure(
            justify="center",
            textvariable=self.ratio,
            width=10)
        self.Entry_r.grid(column=1, row=2)
        self.Entry_c = ttk.Entry(Labelframe_1)
        self.center_edge_dist = tk.DoubleVar()
        self.Entry_c.configure(
            justify="center",
            textvariable=self.center_edge_dist,
            width=10)
        self.Entry_c.grid(column=1, row=3)
        self.Entry_z = ttk.Entry(Labelframe_1)
        self.misallign = tk.DoubleVar()
        self.Entry_z.configure(
            justify="center",
            textvariable=self.misallign,
            width=10)
        self.Entry_z.grid(column=1, row=4)
        self.Radiobutton_dr = ttk.Radiobutton(Labelframe_1)
        self.selected_button = tk.IntVar(value=0)
        self.Radiobutton_dr.configure(
            text='DR', value=0, variable=self.selected_button)
        self.Radiobutton_dr.grid(column=1, row=5, sticky="w")
        self.Radiobutton_mammo = ttk.Radiobutton(Labelframe_1)
        self.Radiobutton_mammo.configure(
            text='MAMMO', value=1, variable=self.selected_button)
        self.Radiobutton_mammo.grid(column=0, row=5)
        self.Button_calculate_sid = ttk.Button(Labelframe_1)
        self.Button_calculate_sid.configure(text='Calculate')
        self.Button_calculate_sid.grid(column=0, columnspan=2, row=6)
        Labelframe_1.grid(column=0, pady=10, row=1, rowspan=3)
        Labelframe_1.rowconfigure("all", pad=20)
        Labelframe_1.columnconfigure("all", pad=10)
        Labelframe_2 = ttk.Labelframe(Toplevel_1)
        Labelframe_2.configure(height=200, text='Edge Cut-off', width=200)
        Label_6 = ttk.Label(Labelframe_2)
        Label_6.configure(text='Edge Lost (Left):')
        Label_6.grid(column=0, row=0, sticky="e")
        Label_7 = ttk.Label(Labelframe_2)
        Label_7.configure(state="normal", text='Edge Lost (Right):')
        Label_7.grid(column=0, row=1, sticky="e")
        Entry_7 = ttk.Entry(Labelframe_2)
        self.cutoff_left = tk.DoubleVar()
        Entry_7.configure(
            justify="center",
            state="readonly",
            takefocus=False,
            textvariable=self.cutoff_left,
            width=10)
        Entry_7.grid(column=1, row=0, sticky="e")
        Entry_8 = ttk.Entry(Labelframe_2)
        self.cutoff_right = tk.DoubleVar()
        Entry_8.configure(
            justify="center",
            state="readonly",
            takefocus=False,
            textvariable=self.cutoff_right,
            width=10)
        Entry_8.grid(column=1, row=1, sticky="e")
        Label_10 = ttk.Label(Labelframe_2)
        Label_10.configure(text='%')
        Label_10.grid(column=2, row=0, sticky="w")
        Label_11 = ttk.Label(Labelframe_2)
        Label_11.configure(text='%')
        Label_11.grid(column=2, row=1, sticky="w")
        Labelframe_2.grid(column=1, row=1)
        Labelframe_2.rowconfigure("all", pad=20)
        Labelframe_2.columnconfigure("all", pad=10)
        Labelframe_3 = ttk.Labelframe(Toplevel_1)
        Labelframe_3.configure(
            height=200,
            text='Applicable Range\n',
            width=200)
        Label_8 = ttk.Label(Labelframe_3)
        Label_8.configure(text='SID_min (cm):')
        Label_8.grid(column=0, row=0, sticky="e")
        Label_9 = ttk.Label(Labelframe_3)
        Label_9.configure(text='SID_Max (cm):')
        Label_9.grid(column=0, row=1, sticky="e")
        Entry_9 = ttk.Entry(Labelframe_3)
        self.sid_min = tk.DoubleVar()
        Entry_9.configure(
            justify="center",
            state="readonly",
            takefocus=False,
            textvariable=self.sid_min,
            width=10)
        Entry_9.grid(column=1, row=0)
        Entry_10 = ttk.Entry(Labelframe_3)
        self.sid_max = tk.DoubleVar()
        Entry_10.configure(
            justify="center",
            state="readonly",
            takefocus=False,
            textvariable=self.sid_max,
            width=10)
        Entry_10.grid(column=1, row=1)
        Labelframe_3.grid(column=1, row=2)
        Labelframe_3.rowconfigure("all", pad=20)
        Labelframe_3.columnconfigure("all", pad=20)
        self.Button_exit = ttk.Button(Toplevel_1)
        self.Button_exit.configure(takefocus=False, text='Exit')
        self.Button_exit.grid(column=1, row=3)
        Labelframe_4 = ttk.Labelframe(Toplevel_1)
        Labelframe_4.configure(height=200, text='Moiré Pattern', width=200)
        Label_12 = ttk.Label(Labelframe_4)
        Label_12.configure(
            justify="center",
            text='Grid Line Densigty:\n(lp/cm)')
        Label_12.grid(column=0, row=0, sticky="e")
        Label_13 = ttk.Label(Labelframe_4)
        Label_13.configure(text='Grid Pitch (μm):')
        Label_13.grid(column=0, row=1, sticky="e")
        Label_14 = ttk.Label(Labelframe_4)
        Label_14.configure(justify="center",
                           text='Sampling Frequency:\n(pixels/cm)')
        Label_14.grid(column=2, row=0, sticky="e")
        Label_15 = ttk.Label(Labelframe_4)
        Label_15.configure(text='Pixel Size (μm):')
        Label_15.grid(column=2, row=1, sticky="e")
        Label_16 = ttk.Label(Labelframe_4)
        Label_16.configure(
            justify="center",
            text='Moiré Frequency\n(fringes/cm):')
        Label_16.grid(column=4, row=0, sticky="e")
        Label_17 = ttk.Label(Labelframe_4)
        Label_17.configure(justify="center", text='Moiré Fringe Period\n(cm):')
        Label_17.grid(column=4, row=1, sticky="e")
        self.Entry_ug = ttk.Entry(Labelframe_4)
        self.u_g = tk.DoubleVar()
        self.Entry_ug.configure(
            justify="center",
            textvariable=self.u_g,
            width=10)
        self.Entry_ug.grid(column=1, row=0)
        self.Entry_us = ttk.Entry(Labelframe_4)
        self.u_s = tk.DoubleVar()
        self.Entry_us.configure(
            justify="center",
            textvariable=self.u_s,
            width=10)
        self.Entry_us.grid(column=3, row=0)
        Entry_3 = ttk.Entry(Labelframe_4)
        self.u_m = tk.DoubleVar()
        Entry_3.configure(
            justify="center",
            state="readonly",
            takefocus=False,
            textvariable=self.u_m,
            width=10)
        Entry_3.grid(column=5, row=0)
        self.Entry_pg = ttk.Entry(Labelframe_4)
        self.p_g = tk.DoubleVar()
        self.Entry_pg.configure(
            justify="center",
            textvariable=self.p_g,
            width=10)
        self.Entry_pg.grid(column=1, row=1)
        self.Entry_ps = ttk.Entry(Labelframe_4)
        self.p_s = tk.DoubleVar()
        self.Entry_ps.configure(
            justify="center",
            textvariable=self.p_s,
            width=10)
        self.Entry_ps.grid(column=3, row=1)
        Entry_6 = ttk.Entry(Labelframe_4)
        self.p_m = tk.DoubleVar()
        Entry_6.configure(
            justify="center",
            state="readonly",
            takefocus=False,
            textvariable=self.p_m,
            width=10)
        Entry_6.grid(column=5, row=1)
        self.Button_calculate_moire = ttk.Button(Labelframe_4)
        self.Button_calculate_moire.configure(text='Calculate')
        self.Button_calculate_moire.grid(column=2, columnspan=4, row=2)
        Label_22 = ttk.Label(Labelframe_4)
        Label_22.configure(text='Theta(°):')
        Label_22.grid(column=0, row=2, sticky="e")
        self.Entry_theta = ttk.Entry(Labelframe_4)
        self.theta = tk.DoubleVar()
        self.Entry_theta.configure(
            justify="center",
            textvariable=self.theta,
            width=10)
        self.Entry_theta.grid(column=1, row=2)
        Labelframe_4.grid(column=0, columnspan=2, padx=10, pady=10, row=0)
        Labelframe_4.rowconfigure("all", pad=20)
        Labelframe_4.columnconfigure("all", pad=10)

        # Main widget
        self.mainwindow = Toplevel_1

    def run(self):
        self.mainwindow.mainloop()


if __name__ == "__main__":
    app = GuiApp()
    app.run()
